package com.example.bridgehands

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import java.util.*
import kotlin.math.floor

class MainActivity : AppCompatActivity() {
    val input = "NCQDTC4D8S7HTDAH7D2S3D6C6S6D9S4SAD7H2CKH5D3CTS8C9H3C3DQS9SQDJH8HAS2SKD4H4S5C7SJC8DKC5C2CAHQCJSTH6HKH9D5HJ#"
    val NPlayer = BooleanArray(52)
    val SPlayer = BooleanArray(52)
    val EPlayer = BooleanArray(52)
    val WPlayer = BooleanArray(52)
    var count = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //readDefinitionFile()
        //Log.d("NPlayer", NPlayer.contentToString())
        splitInput(input)
        returnHands()
    }

    private fun splitInput(string: String){
        for(i in 1 until string.length-1 step 2)
        {
            val subS = string.substring(i, i+2)
            val arrayPos = translateToArray(subS)
            insertInArray(arrayPos)
        }
    }

    private fun returnHands()
    {
        val SindexArray = changeToIndices(SPlayer)
        val sList = mutableListOf<String>()
        for (i in SindexArray.indices){
            val string = translateToSuitRank(SindexArray[i])
            sList.add(i, string)
        }
        println("S: " + sList.toString().substring(1, sList.toString().length-1))

        val WindexArray = changeToIndices(WPlayer)
        val wList = mutableListOf<String>()
        for (i in WindexArray.indices){
            val string = translateToSuitRank(WindexArray[i])
            wList.add(i, string)
        }
        println("W: " + wList.toString().substring(1, wList.toString().length-1))

        val NindexArray = changeToIndices(NPlayer)
        val nList = mutableListOf<String>()
        for (i in NindexArray.indices){
            val string = translateToSuitRank(NindexArray[i])
            nList.add(i, string)
        }
        println("N: " + nList.toString().substring(1, nList.toString().length-1))

        val EindexArray = changeToIndices(EPlayer)
        val eList = mutableListOf<String>()
        for (i in EindexArray.indices){
            val string = translateToSuitRank(EindexArray[i])
            eList.add(i, string)
        }
        println("E: " + eList.toString().substring(1, eList.toString().length-1))
    }

    private fun insertInArray(int: Int)
    {
        when (count%4) {
            0 -> EPlayer[int] = true
            1 -> SPlayer[int] = true
            2 -> WPlayer[int] = true
            3 -> NPlayer[int] = true
        }
        count++
    }

    private fun translateToArray(string: String) : Int
    {
        val rankPos = "23456789TJQKA".indexOf(string.elementAt(1))
        val suitPos = "CDSH".indexOf(string.elementAt(0))
        return (suitPos*13)+rankPos
    }

    private fun translateToSuitRank(int: Int) : String
    {
        val suitPos = int/13
        val suit = "CDSH".elementAt(suitPos)
        val rankPos = int-suitPos*13
        val rank = "23456789TJQKA".elementAt(rankPos)
        return suit.toString()+rank.toString()
    }

    private fun changeToIndices(array: BooleanArray) : IntArray
    {
        var curr = 0
        val theArray = IntArray(13)
        for (i in 0 until array.size)
        {
            //Log.d("test", "$i $curr")
            if (array[i]) {
                theArray[curr] = i
                curr++
            }
        }
        return theArray
    }

//    private fun readDefinitionFile() {
//        val input = Scanner(getResources().openRawResource(R.raw.Hands))
//        while (input.hasNextLine()) {
//            val result = input.nextLine().split("-")
//            val word = result[0].trim()
//            val definition = result[1].trim()
//
//            wordsToDefinitions[word] = definition
//        }
//    }

}
